;;; shrink-path.el --- fish-style path -*- lexical-binding: t; -*-

;; Copyright (C) 2017 Benjamin Andresen

;; Author: Benjamin Andresen
;; Version: 0.3.1
;; URL: https://gitlab.com/bennya/shrink-path.el
;; Package-Requires: ((emacs "24") (s "1.6.1") (dash "1.8.0") (f "0.10.0"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file LICENSE.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:

;; Provides functions that offer fish shell[1] path truncation.
;; Directory /usr/share/emacs/site-lisp => /u/s/e/site-lisp
;;
;; Also includes utility functions that make integration in eshell or the
;; modeline easier.
;;
;; [1] https://fishshell.com/


;;; Code:
(require 'dash)
(require 's)
(require 'f)
(eval-when-compile
  (require 'rx)
  (require 'pcase))

(defun shrink-path--truncate (str)
  "Return STR's first character or first two characters if hidden."
  (substring str 0 (if (s-starts-with? "." str) 2 1)))

(defun shrink-path--dirs-internal (full-path &optional truncate-all)
  "Return fish-style truncated string based on FULL-PATH.
Optional parameter TRUNCATE-ALL will cause the function to truncate the last
directory too."
  (let* ((path (replace-regexp-in-string
                (s-concat "^" (getenv "HOME")) "~" full-path))
         (split (s-split "/" path 'omit-nulls))
         (split-end (1- (length split)))
         (shrunk
          (->> split
               (--map-indexed
                (if (= it-index split-end)
                    (if truncate-all (shrink-path--truncate it) it)
                  (shrink-path--truncate it)))
               (s-join "/"))))
    (s-concat
     (unless (s-matches? (rx bos (or "~" "/")) shrunk) "/")
     shrunk
     (unless (s-ends-with? "/" shrunk) "/"))))


(defun shrink-path-dirs (&optional path truncate-tail)
  "Given PATH return fish-styled shrunken down path.
TRUNCATE-TAIL will cause the function to truncate the last directory too."
  (pcase (f-full (or path default-directory))
    ((app f-short "/") "/")
    ;; I'm confused by what this is supposed to do
    ;; the original `cond' clause is ((s-matches? (rx bos (or "~" "/") eos) "~/"))
    ;; which would always return nil, so must be a typo.
    ;; But even if this is supposed to be matching (f-full path) then there
    ;; shouldn't be a tilde for $HOME, so it will probably fail in that case too.
    ((rx bos (or "~" "~/") eos) "~/")
    (it (shrink-path--dirs-internal it truncate-tail))))

(defun shrink-path-expand (str &optional absolute-p)
  "Return expanded path from STR if found or list of matches on multiple.
The path referred to by STR has to exist for this to work.
If ABSOLUTE-P is non-nil the returned path will be absolute."
  (-let [(head . rest) (s-split "/" str 'omit-nulls)]
    (if (null rest)
        (s-concat "/" head)
      (-let* [(last (-last-item rest))
              (str (->> rest
                        (--map (s-concat it "*"))
                        -butlast ;; drop last as it may not exist
                        (s-join "/")
                        (s-concat (if (s-equals? head "~") "~/" head))
                        f-glob
                        (--map (s-concat it "/" last))
                        (-map (if absolute-p #'f-full #'f-abbrev))))]
        (if (cdr str) str (car str))))))

(defun shrink-path-prompt (&optional pwd)
  "Return cons of BASE and DIR for PWD.
If PWD isn't provided will default to `default-directory'."
  (-let [shrunk (shrink-path-dirs (or pwd default-directory))]
    (-if-let (dir (-last-item (s-split "/" shrunk 'omit-nulls)))
        (-> dir
            (s-concat "/")
            (s-chop-suffix shrunk)
            (cons dir))
      '("" . "/"))))

(defun shrink-path-file (file &optional truncate-tail)
  "Return FILE's shrunk down path and filename.
TRUNCATE-TAIL controls if the last directory should also be shortened."
  (-> file
      f-dirname
      (shrink-path-dirs truncate-tail)
      (s-concat (f-filename file))))

(defun shrink-path-file-expand (str &optional exists-p absolute-p)
  "Return STR's expanded filename.
The path referred to by STR has to exist for this to work.
If EXISTS-P is t the filename also has to exist.
If ABSOLUTE-P is t the returned path will be absolute."
  (let ((expanded (shrink-path-expand str absolute-p)))
    (if (and expanded exists-p)
        (if (f-exists? expanded) expanded)
      expanded)))

(defun shrink-path-file-mixed (shrink-path rel-path filename)
  "Returns list of mixed truncated file name locations.

Consists of SHRINK-PATH's parent, SHRINK-PATH basename, relative REL-PATH and
FILENAME.
For use in modeline or prompts, etc."
  (when (f-descendant-of? filename shrink-path)
    (-let* [((sp-parent . sp-rel) (shrink-path-prompt shrink-path))
            (rel-rel (unless (f-same? rel-path shrink-path)
                       (f-relative rel-path shrink-path)))]
      (list sp-parent sp-rel
            (unless (equal rel-rel ".") rel-rel)
            (file-name-nondirectory filename)))))

(provide 'shrink-path)
;;; shrink-path.el ends here
